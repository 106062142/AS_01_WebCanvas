# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
Canvas功能介紹(106062142黃晨瑋)
1. Default Setting
整個網頁之背景更改為一張風景照
背景：白色、顏色：黑色、大小：20px、筆刷：實線、圖案格式：實心、字體：Arial。另外，在瀏覽器上開啟時，分頁之icon為一支畫筆
2. Resize
畫布(canvas)會按照螢幕大小自動調整大小，並將顏色及大小恢復為預設值
3. Hover & Clcik
將游標移動到上方框框時會改變其顏色，點選時也會有顏色變化；右方顏色條則是會有白框顯示在當前使用之顏色上
4. Cursor
預設為十字架形狀，若將游標移動到上方框框時，會變成指標狀；而打字時則為I字型的輸入文字形狀
5. Color
右上方共有14種顏色可供點選，只要將滑鼠移過去點擊即可。任何樣式、背景、字體之顏色皆會依照此設定
6. Style
針對圓形、長方形及三角形，有實心(solid)及空心(hollow)兩種模式供選擇。
字體部分，則有普通(normal)及斜體(italic)，字體則有三種：Arial、Courier New、Times New Roman
7. Features
背景顏色：先選好顏色再點擊Background Color，即可更換背景顏色
Type：打字(英文/注音符號)，只要在畫布上任意位置點擊滑鼠，即可在該位置打字(排除Alt、Ctrl、Tab等並非輸入字元的按鍵)
Pen：畫出線條(預設值)
Spot：畫出"點點線"
Special Line1：跟後者的差異為：後者是點點狀，每個點都有不同樣式；這個是線狀，每個線條會有同一樣式、不同線會有不同樣式
Special Line2：畫畫的過程中，顏色&濃度&彩度會隨機變化，成為一繽紛的彩色點點線條
Eraser：可將畫上去的內容擦掉
Rectangle：畫出實心/空心長方形
Triangle：畫出實心/空心三角形
Circle：畫出實心/空心圓形
Smile：畫出笑臉
8. Radius
改變大小，包含線條粗細、點點大小、幾何圖形大小、笑臉大小，範圍為0.5 ~ 80(每次改變5個單位)
9. Undo & Redo
上一步、下一步
10. Download
下載畫布，可將所繪之canvas下載
11. Upload
可上傳圖片成為背景。若太小會對齊左上方；太大則會將長/寬按比例縮小，以較小的一邊能剛好容納為準
12. Reset
清空畫布，並將顏色、背景顏色、大小、字體設定恢復為預設值
