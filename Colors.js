// setSwatch( {target: document.getElementsByClassName('clr')[13]} ); // Assign the default swatch. Done in main.js

var pallet = ['red', 'pink', 'orange', 'yellow', 'lightgreen', 'green', 'blue',
              'indigo', 'blueviolet', 'goldenrod', 'sienna', 'white', 'grey','black']

for(var i = 0, n = pallet.length; i < n; i++){
    var swatch = document.createElement('div');
    swatch.className = 'clr';
    swatch.style.backgroundColor = pallet[i];
    swatch.addEventListener('click', setSwatch);

    document.getElementById('colors').appendChild(swatch);
}

function setColor(color){
    ctx.fillStyle = color;
    ctx.strokeStyle = color; // ctx.strokeStyle = $('#selColor').val();
    ctx.lineCap = 'round'; // In case something went wrong
}

// To change color this way, CSS background-color should set in .html but not .css
// (If hard-coded in .html, but now they are created in the top of this file)
function setSwatch(e){
    // window.alert( e + "!!" + e.target + "!!" + e.target.style + "!!" + e.target.style.backgroundColor + "!!"); // this === e.target
    // [object Object]!![object HTMLDivElement]!![object CSSStyleDeclaration]!!red!!

    // Identify Swatch
    var swatch =  e.target;
    // Set Color
    setColor(swatch.style.backgroundColor);
    // Deal with previous active color
    var active = document.getElementsByClassName('active')[0]; // There should only be one active element
    if( active ) active.className = 'clr';
    // Give Active Class
    swatch.className += ' active';
}