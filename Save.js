var dnldButton = document.getElementById('download');

dnldButton.addEventListener('click', saveImage);


function saveImage(){
    // Forcing download on the front-end side using the download attribute is a young feature, supported only by Chrome and Firefox.
    // Ver.1
    // dnldButton.download = "Canvas_image.png"; // Set the name of download file
    // dnldButton.href = canvas.toDataURL(); // Retrieve the image data from the canvas element itself
    
    // Ver.2
    /*
        Some browsers will only allow an anchor element to be activated if it resides within the body of your HTML document
        To get around this you can temporarily add it to the document, activate the link, then immediately remove it.
        This will not be visible to the user as it happens almost instantly.
    */
    var tmpLink = document.createElement( 'a' );
    tmpLink.download = 'image.png';
    tmpLink.href = canvas.toDataURL();
    document.body.appendChild( tmpLink );
    tmpLink.click();
    document.body.removeChild( tmpLink );
 
    // Ver.3
    // var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream"); // "image/png" is default value
    // If you dont replace you will get a DOM 18 exception!!!!
    // window.location.href = image; // OR window.location.href = image

    // console.log(window.location); // Location {replace: ƒ, href: "file:///C:/Users/cwHuang/Desktop/Canvas/canvas01.html", ancestorOrigins...}
    // console.log(window.location.href); // file:///C:/Users/cwHuang/Desktop/Canvas/canvas01.html
}

// function saveImage(){
//     var data = canvas.toDataURL();
//     var request = new XMLHttpRequest();

//     console.log("1.");
//     request.onreadystatechange = function(){
//         console.log("2." + request + " !!!! " + this + " !!!! ");

//         if( request.readyState == 4 && request.status == 200 ){
//             // console.log("3." + this.status + "@@@@@" + request.status + "@@@@@"); // status == 0 in chrome
//             //Save.js:29 Access to XMLHttpRequest at 'file:///C:/Users/cwHuang/Desktop/Canvas/save.php' from 
//             // origin 'null' has been blocked by CORS policy: Cross origin requests are only supported for 
//             // protocol schemes: http, data, chrome, chrome-extension, https.

//             var response = request.responseText;

//             console.log("4." + response + "!!!!!");
//             window.open(response, '_blank', 'location=0, menubar=0');
//             // window.open('download.php?file=' + response, '_blank', 'location=0, menubar=0');
//             console.log("5. !!!!!");
//         }
//     }
    
//     request.open('POST', 'save.php', true);
//     request.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
//     console.log("666");
//     request.send('img='+data);
//     console.log("777");
// }
