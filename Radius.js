var MINRAD = 0.5;
var MAXRAD = 80;
var DEFAULTRAD = 20;
var rad_interval = 5;
var radSpan = document.getElementById('rad_val'); // OR $("#rad_val");
var decRad = document.getElementById('dec_rad');
var incRad = document.getElementById('inc_rad');

var setRadius = function(newRad){
    if( newRad <= MINRAD ) newRad = MINRAD;
    else if( newRad > MINRAD && newRad < 6 ) newRad = 5;
    else if( newRad >= MAXRAD) newRad = MAXRAD;
    Radius = newRad;
    ctx.lineWidth = Radius;
    
    if ( radSpan.textContent ) radSpan.textContent = Radius; // Some browser doesn't suppose all these things
    else radSpan.innerText = Radius;
    // radSpan.textContent = Radius;
    // radSpan.innerHTML = Radius; // using innerHTML can subject you to content injection attacks like XSS
}


decRad.addEventListener('click', () => setRadius( Radius - rad_interval ))
incRad.addEventListener('click', () => setRadius( Radius + rad_interval ))
// setRadius(DEFAULTRAD); // Done in main.js

// Done by Bootstrap
// decRad.addEventListener('mouseover', (e) => e.target.style.backgroundColor = "rgba(216, 209, 209, 0.945)");
// incRad.addEventListener('mouseover', (e) => e.target.style.backgroundColor = "rgba(216, 209, 209, 0.945)");
// decRad.addEventListener('mouseout', (e) => e.target.style.backgroundColor = "#696868");
// incRad.addEventListener('mouseout', (e) => e.target.style.backgroundColor = "#696868");