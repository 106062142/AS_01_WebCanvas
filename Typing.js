var style = "normal"; // italic, oblique, normal
var family = "Arial"; // Times New Roman, Courier New, sans-serif, serif
var typeButton = document.getElementById('type00');
var style_nromal = document.getElementById('font_normal');
var style_italic = document.getElementById('font_italic');
var style_arial = document.getElementById('font_arial');
var style_couriernew = document.getElementById('font_couriernew');
var style_timesnewroman = document.getElementById('font_timesnewroman');

style_nromal.addEventListener('click', () => style = "normal");
style_italic.addEventListener('click', () => style = "italic");
style_arial.addEventListener('click', () => family = "Arial");
style_couriernew.addEventListener('click', () => family = "Courier New");
style_timesnewroman.addEventListener('click', () => family = "Times New Roman");

typeButton.addEventListener('click', () => {
    if( startTyping ){
        startTyping = false;
        document.getElementById("art").style.cursor = "crosshair";
    } else{
        startTyping = true;
        document.getElementById("art").style.cursor = "text";
    }
})

var cnt = 0, select = 0; // Important !!!!!!
art.onmousedown = function (e) {
    if( startTyping == false ) return;
    cnt++;
    // console.log("Typing: " + cnt + "!!!!");
    
    var nameBuffer='';
    var startx = e.clientX - canvas.getBoundingClientRect().left - 3; // OR e.pageX - this.offsetLeft;//this:canvas
    var starty = e.clientY - canvas.getBoundingClientRect().top;

    size = `${Radius}px`;
    ctx.font = `${style} ${size} ${family}`; // OR String(style) + " " + String(size) + " " + String(family)

    function draw() {
        ctx.fillText(nameBuffer, startx, starty);
    }

    var eve = e; // for 1.
    // $(document).ready( function() { // Redundunt
        // 1
        // $(document).keydown(function(e) {
        //     var keycode = parseInt(e.which); // e.which is as same as e.keyCode, and they are deprecated. But key is not woking
        //     //delete or backspace
        //     if (keycode == 46 || keycode == 8) {
        //         console.log("preventDefault !!!! " + eve);
        //         eve.preventDefault(); //prevent back navigation from backspace
        //         nameBuffer = nameBuffer.slice(0, nameBuffer.length - 1);
        //         draw();
        //     }
        // });
        // 2
            $(document).keypress(function(e) {
                if( startTyping == false ) return; // NECESSARY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                select++;
                // console.log("parseInt: " + select + "@@@@");
                if( select != cnt ) return;
                select = 0;

                var keycode = parseInt(e.which);
                nameBuffer += String.fromCharCode(keycode); // Transform numbers to chararcters
                draw();
            });
    // });

    draw();
}