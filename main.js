// const canvas = document.querySelector("#art");
var canvas = document.getElementById("art");
var ctx = canvas.getContext("2d");
let DRAGGING = false;
var RecordImgHist = new Array(), RecordImgIdx = -1, RecordImgLat = -2;
var Undo = document.getElementById("undo");
var Redo = document.getElementById("redo");
var Reset = document.getElementById('reset');

var Radius = 20;
var startTyping = false;
canvas.height = window.innerHeight * 0.9; // This should go first !!!!!!!! Otherwise radius will be set to 1 by system
canvas.width = window.innerWidth * 0.9; // In html5 spec defines that this function will clear the image

window.addEventListener("resize", toResize);
canvas.addEventListener('mousedown', mouseDown);
canvas.addEventListener('mouseup', mouseUp);
canvas.addEventListener('mousemove', mouseMove);
Undo.addEventListener('click', toUndo);
Redo.addEventListener('click', toRedo);
Reset.addEventListener('click', toReset);

function DefaultSetting(){
    ctx.fillStyle = 'black';
    ctx.strokeStyle = 'black';
    ctx.lineCap = 'round';
    canvas.style.backgroundColor = 'white';
    ctx.lineWidth = 20;
    Radius = 20;
    rad = document.getElementById('rad_val');
    rad.textContent = Radius;
    ctx.font = 'normal 20px Arial'; // Unnecessary, cause this will be run every time before drawing text
}

DefaultSetting();

// Ver.1 Resize
function toResize(){
    var image = ctx.getImageData(0, 0, canvas.width, canvas.height);

    canvas.height = window.innerHeight * 0.9;
    canvas.width = window.innerWidth * 0.9;
    ctx.putImageData(image, 0, 0);
    // setRadius(DEFAULTRAD);
    // setSwatch( {target: document.getElementsByClassName('clr')[0]} );
    DefaultSetting();
};
// Ver.2 Resize
// window.onresize = () => {
//     canvas.width = window.innerWidth * 0.9;
//     canvas.height = window.innerHeight * 0.9;
// }

function mouseDown(e){
    // console.log(SelFeature + " down!!!!!");
    DRAGGING = true;
    selectFeatures(e);
};
function mouseUp(){
    // console.log(SelFeature + " up@@@@@");
    DRAGGING = false;
    ctx.beginPath(); // beginPath() method begins a path, or resets the current path.
    
    RecordImgIdx++;
    RecordImgHist[RecordImgIdx] = ctx.getImageData(0, 0, canvas.width, canvas.height);
    RecordImgLat = RecordImgIdx;
};
function mouseMove(e){
    // console.log(SelFeature + " move#####");
    selectFeatures(e);
};
function toReset(){
    canvas.width = canvas.width; // Enough
    DefaultSetting();
}
// document.getElementById('reset').addEventListener('click', function() {
//     ctx.clearRect(0, 0, canvas.width, canvas.height);
//   }, false);

function toUndo(){
    if( RecordImgIdx < 0 ) return; // -1: Initial State
    else{
        RecordImgIdx--;
        if( RecordImgIdx == -1 ) canvas.width = canvas.width;
        else ctx.putImageData(RecordImgHist[RecordImgIdx], 0, 0);
    }
}
function toRedo(){
    if( RecordImgIdx < RecordImgLat ){
        RecordImgIdx++;
        ctx.putImageData(RecordImgHist[RecordImgIdx], 0, 0);
    }
}
