bkgrdclr = document.getElementById('backgroundcolor');
bkgrdclr.addEventListener('click', () => {
    var active = document.getElementsByClassName('active')[0];
    canvas.style.backgroundColor = active.style.backgroundColor;
})

ctx.lineCap = 'round';
var mousePos;
var SelFeature = 'line';
var hollow_or_solid = 'solid';

var hollow = document.getElementById('hollow');
var solid = document.getElementById('solid');
hollow.addEventListener('click', () => hollow_or_solid = 'hollow' );
solid.addEventListener('click', () => hollow_or_solid = "solid" );
var FeatPen = document.getElementById('pen');
var FeatSpt = document.getElementById('spot');
var FeatSpl_1 = document.getElementById('special_line_1');
var FeatSpl_2 = document.getElementById('special_line_2');
var FeatEra = document.getElementById('eraser');
var FeatRec = document.getElementById('rectangle');
var FeatTri = document.getElementById('triangle');
var FeatCir = document.getElementById('circle');
var FeatSml = document.getElementById('smile');
FeatPen.addEventListener('click', () => { startTyping = false; SelFeature = 'line' });
FeatSpt.addEventListener('click', () => { startTyping = false; SelFeature = 'spotted_line' });
FeatSpl_1.addEventListener('click', () => { startTyping = false; SelFeature = 'gradient_line_1' });
FeatSpl_2.addEventListener('click', () => { startTyping = false; SelFeature = 'gradient_line_2' });
FeatEra.addEventListener('click', () => { startTyping = false; SelFeature = 'eraser' });
FeatRec.addEventListener('click', () => { startTyping = false; SelFeature = 'rectangle' });
FeatTri.addEventListener('click', () => { startTyping = false; SelFeature = 'triangle' });
FeatCir.addEventListener('click', () => { startTyping = false; SelFeature = 'circle' });
FeatSml.addEventListener('click', () => { startTyping = false; SelFeature = 'smile' });

function getMousePos(evt) {
    var rect = canvas.getBoundingClientRect();
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    }
}

function selectFeatures(e){
    if( !DRAGGING || startTyping == true ) return;

    mousePos = getMousePos(e);
    if( SelFeature == 'eraser' ) draw_erase(mousePos);
    else if( SelFeature == 'line' ) draw_line(mousePos);
    else if( SelFeature == 'gradient_line_1' ) draw_gradient_line(mousePos, 1);
    else if( SelFeature == 'gradient_line_2' ) draw_gradient_line(mousePos, 2);
    else if( SelFeature == 'spotted_line' ) draw_spotted_line(mousePos);
    else{
        ctx.lineWidth /= 2;
        if( SelFeature == 'circle' && hollow_or_solid == "solid" ) draw_fill_circle(mousePos);
        else if (SelFeature == 'circle' && hollow_or_solid == "hollow" ) draw_stroke_circle(mousePos);
        else if (SelFeature == 'rectangle' && hollow_or_solid == "solid" ) draw_fill_rectangle(mousePos);
        else if (SelFeature == 'rectangle' && hollow_or_solid == "hollow" ) draw_stroke_rectangle(mousePos);
        else if (SelFeature == 'triangle' && hollow_or_solid == "solid" ) draw_fill_triangle(mousePos);
        else if (SelFeature == 'triangle' && hollow_or_solid == "hollow" ) draw_stroke_triangle(mousePos);
        else if( SelFeature == 'smile' ) draw_smile(mousePos);
        else if( SelFeature == 'gradient_circle' ) draw_gradient_circle(mousePos);
        else window.alert("Something wrong in selectFeatures");
        ctx.lineWidth *= 2;
        DRAGGING = false;
    }
}

function draw_erase(e){
    // var rec_color = ctx.strokeStyle;
    // ctx.strokeStyle = canvas.style.backgroundColor;
    // ctx.lineTo(e.x, e.y);
    // ctx.stroke();
    // ctx.moveTo(e.x, e.y);
    // ctx.strokeStyle = rec_color;

    ctx.globalCompositeOperation = 'destination-out'; // !!!!!!!!!!
    ctx.lineTo(e.x,e.y);
    ctx.lineJoin = ctx.lineCap = 'round';
    ctx.stroke();
    ctx.moveTo(e.x,e.y);
    ctx.globalCompositeOperation = 'source-over';
}
function draw_line(e){
    ctx.lineTo(e.x, e.y); // defines the ending point of the line
    ctx.stroke();
    ctx.moveTo(e.x, e.y); // defines the starting point of the line
}
var hue = 0, saturation = 0, lightness = 20, random = 0;
function draw_gradient_line(e, mode){
    var rec_color = ctx.strokeStyle;
    ctx.strokeStyle = `hsl(${hue}, ${saturation}%, ${lightness}%)`;//將繪出的線顏色指定為hsl(hue, saturation, lighten)
    ctx.lineTo(e.x, e.y);
    ctx.stroke();
    ctx.moveTo(e.x, e.y);
    if( mode == 2 ) ctx.beginPath(); // Necessary

    hue++; //隨著函式執行色相角度增加而改變顏色
    if( saturation > 90 ) saturation = 5;
    else saturation++;
    if( lightness > 90 ) lightness = 20;
    else lightness++;

    random++;
    if( random == 500 ){
        // hue = Math.floor(Math.random() * 101);
        saturation = Math.floor(Math.random() * 96) + 5;
        lightness = Math.floor(Math.random() * 91) + 10;
        random = 0;
    }
    ctx.strokeStyle = rec_color;
}
function draw_spotted_line(e){
    ctx.moveTo(e.x, e.y);
    ctx.lineTo(e.x, e.y);
    ctx.stroke();
}
function draw_stroke_circle(e){
    ctx.arc(e.x, e.y, Radius * 2, 0, 2 * Math.PI);
    ctx.stroke();
}
function draw_fill_circle(e){
    ctx.arc(e.x, e.y, Radius * 2, 0, 2 * Math.PI);
    ctx.fill();
}
function draw_fill_rectangle(e){
    ctx.fillRect(e.x, e.y, 150 + 20 * Math.log(Radius), 100 + 20 * Math.log(Radius));
}
function draw_stroke_rectangle(e){
    ctx.strokeRect(e.x, e.y, 150, 100);
}
function draw_fill_triangle(e){
      ctx.moveTo(e.x, e.y);
      ctx.lineTo(e.x, e.y + Radius * 5);
      ctx.lineTo(e.x + Radius * 5, e.y);
      ctx.fill();
}
function draw_stroke_triangle(e){
      ctx.moveTo(e.x, e.y);
      ctx.lineTo(e.x, e.y + Radius * 5);
      ctx.lineTo(e.x + Radius * 5, e.y);
      ctx.closePath();
      ctx.stroke();
}
function draw_smile(e){ // arc(x, y, r, startAngle, endAngle, counterClockwise = false)
    ctx.arc(e.x, e.y, 60, 0, Math.PI * 2, true); // Outer circle
    ctx.moveTo(e.x + 35, e.y);
    ctx.arc(e.x, e.y, 35, 0, Math.PI);
    ctx.moveTo(e.x - 15, e.y - 25);
    ctx.arc(e.x - 20, e.y - 25, 5, 0, Math.PI * 2);
    ctx.moveTo(e.x + 23, e.y - 25);
    ctx.arc(e.x + 18, e.y - 25, 5, 0, Math.PI * 2);
    ctx.stroke();
    // stroke() and then beginPath() can replace with moveTo()
}
function draw_gradient_circle(e){    
    let grd = ctx.createRadialGradient(e.x, e.y, 0, e.x, e.y, Radius * 2);
    grd.addColorStop(0, 'blue');
    grd.addColorStop(0.25, 'green');
    grd.addColorStop(0.5, 'yellow');
    grd.addColorStop(0.75, 'orange');
    grd.addColorStop(1, 'red');
    ctx.beginPath();
    ctx.fillStyle = grd;
    ctx.arc(e.x, e.y, Radius * 2, 0, 2 * Math.PI);
    ctx.fill();
}
